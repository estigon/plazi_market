package com.platzi.market.domain.service;

import com.platzi.market.domain.Product;
import com.platzi.market.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    /*
    * Podemos utilizar @Autowired a pesar de que la interface ProductRepository no es un componente de spring
    * porque ProductoRepository, que implementa la interface si lo es*/
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAll(){
        return productRepository.getAll();
    }

    public Optional<Product> getProduct(int productId){
        return productRepository.getProduct(productId);
    }

    public Optional<List<Product>> getByCategory(int categoryId){
        return productRepository.getByCategory(categoryId);
    }

    public Product save(Product product){
        return productRepository.save(product);
    }

    public boolean delete(int productId){
        /*return getProduct(productId).map(product -> {
            productRepository.delete(productId);
            return true;
        }).orElse(false);*/

       // otra forma
        /*if(getProduct(productId).isPresent()){
            productRepository.delete(productId);
            return true;
        }else{
            return false;
        }*/

        /*los primeros dos metodos deben hacer primero una busqueda y si consiguen
        el elemento hacer la eliminacion trayendo mas costo maquina*/
        //metodo mas optimo para la eliminacion
        try {
            productRepository.delete(productId);
            return true;
        }catch (EmptyResultDataAccessException e){
            return false;
        }
    }
}
