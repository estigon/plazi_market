package com.platzi.market.persistence.entity;

import javax.persistence.*;
import java.security.PrivateKey;
import java.util.List;

/*
* No usar tipos primitivos, si no usar sus clases wrapper: Integer, Double, Boolean, etc.
* */

/*
    @GeneratedValue// define como se generara la clave primaria de la tabla
    GenerationType.IDENTITY
* Se basa en una columna de base de datos con incremento automático y permite que la base de datos genere
* un nuevo valor con cada operación de inserción. Desde el punto de vista de la base de datos, esto es muy
* eficiente porque las columnas de incremento automático están altamente optimizadas y no requiere ninguna
* declaración adicional.
* */

@Entity//define que se comportara como una clase para mapear datos
@Table(name = "productos")//nombre de la tabla con la que estara vinculada la clase
public class Producto {

    @Id//indica que este atributo es la primary key de la tabla
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_producto")// si el nombre de la columna es diferente al nombre del atributo de la clase debe colocar @Column con el nombre de la columna de la bbdd
    private Integer idProducto;

    private String nombre;

    @Column(name = "id_categoria")
    private Integer idCategoria;

    @Column(name = "codigo_barras")
    private String codigoBarras;

    @Column(name = "precio_venta")
    private Double precioVenta;

    @Column(name = "cantidad_stock")
    private Integer cantidadStock;

    private Boolean estado;

    @ManyToOne
    @JoinColumn(name = "id_categoria", insertable = false, updatable = false)
    private Categoria categoria;

    //esta relacion aunque se puede agregar no la colocaremos porque no nos aporta valor
   /* @OneToMany(mappedBy = "producto")
    private List<ComprasProducto> comprasProductos;

    public List<ComprasProducto> getComprasProductos() {
        return comprasProductos;
    }*/

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public Double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(Double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public Integer getCantidadStock() {
        return cantidadStock;
    }

    public void setCantidadStock(Integer cantidadStock) {
        this.cantidadStock = cantidadStock;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Categoria getCategoria() {
        return categoria;
    }


}
