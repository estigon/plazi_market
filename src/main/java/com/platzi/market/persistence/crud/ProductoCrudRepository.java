package com.platzi.market.persistence.crud;

import com.platzi.market.persistence.entity.Producto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/*
* CrudRepository<Tipo_de_objeto_que_representa_la_tabla, Tipo_de_dato_del_primary_key_del_objeto_de_la_tabla>
* */
public interface ProductoCrudRepository extends CrudRepository<Producto, Integer> {

    //usando QueryMethods se ahorra codigo. Hay que seguir una nomenclatura para crear los metodos
    //Asi podemos obtener las consultas que necesitamos que no se adaptan a metodos que ya tiene CrudRepository
    List<Producto> findByIdCategoria(int idCategoria);

    Optional<List<Producto>> findByCantidadStockLessThanAndEstado(int cantidadStock, boolean estado);



    /*
    Haciendolo la consulta de forma nativa no hay que seguir nomenclatura para la declaracion del nombre del metodo
    solo usar la anotacion @Query

    @Query(value = "SELECT * FROM productos WHERE id_categoria = ?", nativeQuery = true)
    List<Producto> getMyCustomCategoryMethod();
    *
     */
}
