package com.platzi.market.persistence.mapper;

import com.platzi.market.domain.Category;
import com.platzi.market.persistence.entity.Categoria;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    @Mappings({
            @Mapping(source = "idCategoria", target = "categoryId"),
            @Mapping(source = "descripcion", target = "category"),
            @Mapping(source = "estado", target = "active")
    })
    Category toCategory(Categoria categoria);

    //@InheritInverseConfiguration indica que el mappeo que queremos es justo el opuesto del que ya realizamos para no repetir todas las lineas
    @InheritInverseConfiguration
    @Mapping(target = "productos", ignore = true)// le indicamos que ignore productos, ya que ese valor no lo tenemos en Category
    Categoria toCategoria(Category category);
}
